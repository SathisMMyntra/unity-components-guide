import * as babel from 'babel-core';

export function compileCode(code) {
    return babel.transform(
        code,
        {
            presets:
            [require('babel-preset-es2015')
                , require('babel-preset-react')
            ]
        }
    ).code;
}

export function getComponent(code) {
    const compiledCode = compileCode(code);
    return eval(compiledCode);
}