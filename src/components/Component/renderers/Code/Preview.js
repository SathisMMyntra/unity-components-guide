import React, {Component} from 'react';
import ReactDom from 'react-dom';
import * as UnityComponents from 'unity-components';
import {getComponent} from './renderUtils'

global.React = React;
Object.keys(UnityComponents).map((key) => {
    global[key] = UnityComponents[key]
});

global.exports = {};

class Preview extends Component {

    constructor(props) {
        super(props);

        this.executeCode = this.executeCode.bind(this);
    }
    
    shouldComponentUpdate() {
        return false;
    }

    componentWillUnmount() {
        try {
        ReactDom.unmountComponentAtNode(this.refs.mount);
      } catch (e) { 
          console.warn(e);
      }
    }

    executeCode(code) {
      const mountNode = this.refs.mount;

      try {
        ReactDom.unmountComponentAtNode(mountNode);
      } catch (e) {
          console.warn(e);
       }

      try {
        const CodeComponent = getComponent(code);
        const element = React.createElement(CodeComponent, {key: new Date().getTime()});

        ReactDom.render(
          element,
          ReactDom.findDOMNode(mountNode)
        );
      } catch (err) {
        setTimeout(function() {
          ReactDom.render(
            <div className="playgroundError">{err.toString()}</div>,
            mountNode
          );
        }, 500);
      }
    }

    render () {
        return (
            <div ref="mount">
            </div>
        );
    }
}

export default Preview