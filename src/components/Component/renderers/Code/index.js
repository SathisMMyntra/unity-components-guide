import React, {Component} from 'react';
import js from 'highlight.js/lib/languages/javascript';
import Lowlight from 'react-lowlight';
import SmartCodeMirror from 'unity-components/react/components/SmartFormv2/SmartCodeMirror.jsx';
import Preview from './Preview';

const codeMirrorOptions = {
    lineNumbers: true,
    lineWrapping:true,
    extraKeys:{
        "Ctrl-Space": "autocomplete"
    },
    smartIndent:true,
    mode:"text/jsx",
    indentWithTabs:true,
    matchBrackets:true,
    placeholder:"Type here"
}

class CodeRenderer extends Component {
    
    static displayName = 'CodeBlock';
    static propTypes = {
        literal: React.PropTypes.string,
        language: React.PropTypes.string,
        inline: React.PropTypes.bool
    };

    constructor(props) {
        super(props);
        this.state = {code: props.literal, showCode: true};

        this.onChange = this.onChange.bind(this);
        this.toggleCode = this.toggleCode.bind(this);
    }

    renderPreview() {
        if (this.refs.preview && this.refs.preview.executeCode) {
            this.refs.preview.executeCode(this.state.code);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.code != this.state.code)
            this.renderPreview();
    }

   componentDidMount() {
        this.renderPreview();
    }


    onChange(code) {
        this.setState({code: code.value});
    }

    toggleCode() {
        this.setState({showCode: !this.state.showCode});
    }

    render () {
        const {showCode} = this.state
        const arrowClass = showCode ? "fa fa-chevron-up" : "fa fa-chevron-down"

        return (
            <div className="code-block">
                <div className="code-preview">
                    <Preview ref="preview" />
                </div>
                <div className="view-code" onClick={this.toggleCode}>
                    <i className={arrowClass}></i>
                    <span> View Code </span>
                </div>
                { showCode ? (<SmartCodeMirror 
                    onChange={this.onChange} 
                    options={codeMirrorOptions} 
                    label="" 
                    elementId = 'editor' 
                    value={this.state.code}
                    mandatory={false} 
                    regex="" 
                className="containerClass" />) : null}
            </div>
        )
    }
}

export default CodeRenderer