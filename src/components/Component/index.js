import React, {Component} from 'react';
import Page from 'unity-components/react/components/Page.jsx';
import SmartGrid from 'unity-components/react/components/SmartTable/SmartGrid.jsx'
import {PageTitle, PageHeader} from 'unity-components';
import DocConfig from 'unity-components/guide/doc-2.9.24.json';
import ReactMarkDown from 'react-markdown';
import CodeRenderer from './renderers/Code';

const columnMetaData = [
    {
        columnName: 'propName',
        displayName: 'Property Name'
    },
    {
        columnName: 'description',
        displayName: 'Description',
        flexBasis: 200
    },
    {
        columnName: 'type',
        displayName: 'Type'
    },
    {
        columnName: 'defaultValue',
        displayName: 'Default Value'
    }
];

class ComponentPage extends Component {

    getPropsTable(props) {
        if (!props)
            return null;

        const data = Object.keys(props).map((key) => {
            return {
                propName: key,
                defaultValue: props[key].defaultValue ? props[key].defaultValue.value : '-',
                description: props[key].description,
                type: props[key].type ? props[key].type.name : '-'
            };
        });
       
        return (
            <div style={{paddingTop: 20}}>
                <h4>Properties</h4>
                <div style={{minHeight: 200}}>
                    <SmartGrid useHTMLTable={false} columnMetadata={columnMetaData} data={data} showFooter={false} />
                </div>
            </div>
        )
    }

    render () {
        const {routeParams} = this.props;
        const splits = routeParams.splat.split('/');
        const name = splits[splits.length - 1].split('.')[0];
        const data = DocConfig['react/' + routeParams.splat];

        return (
           <Page showBreadCrumb={false}>
                <div className="page-wrapper">
                    <h3>{name}</h3>
                    <ReactMarkDown 
                        source={data.description}
                        renderers={{
                            CodeBlock: CodeRenderer
                        }}
                     />
                    {this.getPropsTable(data.props)}
                </div>
            </Page>
        )
    }
}

export default ComponentPage