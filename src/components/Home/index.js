import React, {Component} from 'react';
import Page from 'unity-components/react/components/Page.jsx';
import {PageTitle, PageHeader} from 'unity-components';
import ReactMarkDown from 'react-markdown';
import CodeRenderer from '../Component/renderers/Code';


class Home extends Component {
    
    render () {
        return (
           <Page showBreadCrumb={false}>
              <div style={{paddingTop: 20}}>
                <div className="head-1" style={{paddingBottom: 10}}>
                    <h3>Unity-Components</h3>
                    <p> This library contains the unity components which is used in building udp, ddp, scm apps. 
                        It is written on top of metronics theme.</p>
                </div>
                <div style={{paddingBottom: 10}}>
                    <h4> How To Install </h4>
                    <code> $ npm install unity-components --save </code>
                </div>
                <div style={{paddingBottom: 10}}>
                    <h4> How To Use </h4>
                    <code> import &#123;App, AppHeader&#125; from 'unity-components' </code>
                </div>
                <div style={{paddingBottom: 10}}>
                    <h4> To add css </h4>
                    <p>In your less file add</p>
                    <code> @import '~unity-components/dist/index.css'; </code>
                </div>
                <div style={{paddingBottom: 10}}>
                    <h4> Issues </h4>
                    <p> 
                        Please file issues you face in 
                        <a href="https://myntra.slack.com/messages/unity-ui-framework/"> #unity-ui-framework  </a>
                        channel.
                    </p>
                </div>
              </div>
            </Page>
        )
    }
}

export default Home