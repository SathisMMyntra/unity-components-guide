import React from 'react';
const context = require.context('unity-components/guide', false, /.json/);

global.versions = context.keys().map((key) => {
	return {
		key,
		label: key.split('doc-')[1].split(".json")[0],
		value: context(key)
	}
});

export function getAppConfig(selectedVersion) {

	const components = Object.keys(selectedVersion.value).map((key) => {
		const splits = key.split('/');
		return {
			key, 
			title: splits[splits.length - 1].split('.')[0], 
			path: key,
			isSearch: true
		}
	});

	return {

		info: {
			appName: "Unity Components Guide",
			appKey: "ucg",
			appBaseUrl: "/"
		},
		dropDowns: [],
		isNavSearch: true,
		nav: [
			{key: "home", title: "Getting Started", icon: "fa fa-home", path: ""},
			{key: "installation",title: "Installation", icon: "fa fa-clock-o", path: "/installation",child:[]},
			{key: "components", title: "Components", toggle: false, icon: "fa fa-building", path: "/components", child: components}
		]
	}
}
