import React, {Component} from 'react'
import {App} from 'unity-components/react/index.js';
import {getAppConfig} from '../config/AppConfig';
import SmartReactSelect from 'unity-components/react/components/SmartFormv2/SmartReactSelect.jsx'; 

class Application extends Component {

    constructor(props) {
        super(props);
        this.state = {selectedVersion: global.versions[0]};

        this.onVersionChange = this.onVersionChange.bind(this);
        this.getVersions = this.getVersions.bind(this);
    }

    onVersionChange(input) {
        this.setState({selectedVersion: input.value});
    }

    getVersions() {
        const {selectedVersion} = this.state;
        return (
            <div style={{display: 'flex', alignItems: 'center'}}>
                <span style={{paddingRight: 10}}>Version : </span>
                <SmartReactSelect
                    className="version-select"
                    name="version"
                    placeholder="Select Version"
                    options={global.versions}
                    onChange={this.onVersionChange}
                    value={selectedVersion}
                    />
            </div>
        )
    }

    render () {
        const {selectedVersion} = this.state;
        const config = {...getAppConfig(selectedVersion), header: { customContent: this.getVersions }};

        return (
            <App {...config}>
				{this.props.children}
			</App>
        )
    }
}

export default Application