import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';

import Home from '../components/Home'
import App from '../containers/App'
import Component from '../components/Component'
import NotFound from '../components/NotFound'

var AppRoutes = [
    <Route component={App}>
        <Route path='/' component={Home} />
        <Route path='/react'>
            <Route path='*' component={Component} />
        </Route>
        <Route path='*' component={NotFound}/>
    </Route>
]

export default AppRoutes;