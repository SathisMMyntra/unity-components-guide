import React from 'react';
import { render } from 'react-dom';
import App from './containers/App';
import { Router, Route, browserHistory } from 'react-router';

import routes from './routes'
import './less/index.less';

render(
  <Router history={browserHistory} routes={routes} />,
  document.getElementById('App')
);

